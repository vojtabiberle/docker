FROM docker:20

RUN set -eux; \
    apk --update add openssh-client curl bash gettext; \
    export bin_path=/usr/local/bin; \
    curl -sfL https://direnv.net/install.sh | sh; \
    chmod +x /usr/local/bin/direnv
